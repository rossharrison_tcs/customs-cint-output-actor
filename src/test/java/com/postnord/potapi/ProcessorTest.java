package com.postnord.potapi;

import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.internal.Constants;
import com.postnord.potapi.internal.TestHelper;
import com.postnord.potapi.processor.Processor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class ProcessorTest {

    private TopologyTestDriver topologyTestDriver;

    @Before
    public void setUp() {
        Properties props = new Properties();
        Processor processor = new Processor();
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "potapi-input-test-client");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "potapi");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "potapi-input-test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 1);
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        props.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        Topology topology = processor.topology();
        topologyTestDriver = new TopologyTestDriver(topology, props);
    }

    @Test
    public void shouldWriteToOutputTopic() throws Exception{
        TransactionResult transactionResult = TestHelper.getSample("transaction-commit-glp.json");
        assertThat(transactionResult).isNotNull();
        writeEventToTopology(transactionResult);

        ProducerRecord<String, String> outputXml = readOutputXML();
        String output = outputXml.value();
        assertThat(output).isNotNull();
    }

    @Test
    public void  shouldPopulateEuFlagToCint() throws  Exception{
        TransactionResult transactionResult = TestHelper.getSample("transaction-commit-glp.json");
        assertThat(transactionResult).isNotNull();
        writeEventToTopology(transactionResult);

        ProducerRecord<String, String> outputXml = readOutputXML();
        String output = outputXml.value();
        assertThat(output).isNotNull();


    }
    private ProducerRecord<String, String> readOutputXML() {
        ProducerRecord<String, String> result = topologyTestDriver.readOutput(
                Constants.PROD_GLP_SE_CUSTOMS_ADDRESS_WRITE, Serdes.String().deserializer(),
                Serdes.String().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private void writeEventToTopology(TransactionResult transactionResult) {

        ConsumerRecordFactory<String, TransactionResult> factory = new ConsumerRecordFactory<>(
                Serdes.String().serializer(),
                com.postnord.orm.commons.serialization.transaction.TransactionSerde.transactionResult().serializer());
        topologyTestDriver.pipeInput(factory.create(Constants.TRANSACTION_COMMIT,
                transactionResult.getId().toString(), transactionResult));
    }

    @After
    public void cleanUp(){
        topologyTestDriver.close();
    }
}
