package com.postnord.potapi.internal;

import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.util.JsonUtil;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public class TransactionSerde implements Serde<Transaction> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void close() {

    }

    @Override
    public Serializer<Transaction> serializer() {
        return new TransactionSerializer();
    }

    @Override
    public Deserializer<Transaction> deserializer() {
        return new TransactionDeserializer();
    }

    private class TransactionSerializer implements Serializer<Transaction> {
        @Override
        public void configure(Map<String, ?> map, boolean b) {
        }

        @Override
        public byte[] serialize(String topic, Transaction data) {
            JsonUtil<Transaction> utils = new JsonUtil<>();
            return utils.toJson(data).getBytes(StandardCharsets.UTF_8);
        }
        @Override
        public void close() {
        }
    }

    private class TransactionDeserializer implements Deserializer<Transaction> {
        @Override
        public void configure(Map<String, ?> map, boolean b) {
        }

        @Override
        public Transaction deserialize(String s, byte[] data) {
            JsonUtil<Transaction> utils = new JsonUtil<>();
            String transaction = new String(data, StandardCharsets.UTF_8);
            return utils.fromJson(transaction, Transaction.class);
        }

        @Override
        public void close() {
        }
    }
}
