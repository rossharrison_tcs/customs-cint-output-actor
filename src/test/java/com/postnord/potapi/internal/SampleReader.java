package com.postnord.potapi.internal;

import com.postnord.orm.commons.serialization.transaction.TransactionSerde;
import com.postnord.orm.service.transaction.model.TransactionResult;
import org.apache.kafka.common.serialization.Serde;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

class SampleReader {

    TransactionResult getSample(String name) throws IOException {
        Serde<TransactionResult> transactionResultSerde = TransactionSerde.transactionResult();

        String transactionCommitSample = getTransactionCommitSample(name);
        byte[] sample = transactionCommitSample.getBytes(StandardCharsets.UTF_8);

        return transactionResultSerde.deserializer().deserialize("", sample);
    }

    private String getTransactionCommitSample(String name) throws IOException {
        String resourceName = "" + name;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(resourceName), StandardCharsets.UTF_8))) {
            return bufferedReader
                    .lines()
                    .collect(Collectors.joining());
        }
    }
}
