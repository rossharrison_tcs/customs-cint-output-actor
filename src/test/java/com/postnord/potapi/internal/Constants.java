package com.postnord.potapi.internal;

public class Constants {

    public static final String PROD_GLP_SE_CUSTOMS_ADDRESS_WRITE = "prod-glp-se-customs-address-write";
    public static final String TRANSACTION_COMMIT = "transaction-commit";
}
