package com.postnord.potapi.internal;

import com.postnord.orm.commons.serialization.transaction.TransactionSerde;
import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.model.util.JsonUtil;
import org.apache.kafka.common.serialization.Serde;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class TestHelper {

    public static InputStream getFileAsStream(String resource) throws IOException {
        File initialFile = new File("src/test/resources/" + resource);
        return new FileInputStream(initialFile);
    }

    private static String getStreamSample(String fileName) throws IOException {

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getFileAsStream(fileName), StandardCharsets.UTF_8))) {
            return bufferedReader.lines().collect(Collectors.joining());
        }
    }

    public static TransactionResult getTransactionResult(String fileName) throws IOException{

        JsonUtil<TransactionResult> utils = new JsonUtil<>();
        TransactionResult transactionResult = utils.fromJson(getStreamSample(fileName), TransactionResult.class);
        return transactionResult;
    }

    public static TransactionResult getSample(String name) throws IOException {
        Serde<TransactionResult> transactionResultSerde = TransactionSerde.transactionResult();

        String transactionCommitSample = getStreamSample(name);
        byte[] sample = transactionCommitSample.getBytes(StandardCharsets.UTF_8);

        return transactionResultSerde.deserializer().deserialize("", sample);
    }

}
