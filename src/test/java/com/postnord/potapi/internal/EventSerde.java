package com.postnord.potapi.internal;

import com.postnord.potapi.model.Event;
import com.postnord.potapi.model.util.JsonUtil;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public class EventSerde implements Serde<Event> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public void close() {

    }

    @Override
    public Serializer<Event> serializer() {
        return new EventSerializer();
    }

    @Override
    public Deserializer<Event> deserializer() {
        return new EventDeserializer();
    }

    private class EventSerializer implements Serializer<Event> {
        @Override
        public void configure(Map<String, ?> map, boolean b) {
        }

        @Override
        public byte[] serialize(String topic, Event data) {
            JsonUtil<Event> utils = new JsonUtil<>();
            return utils.toJson(data).getBytes(StandardCharsets.UTF_8);
        }
        @Override
        public void close() {
        }
    }

    private class EventDeserializer implements Deserializer<Event> {
        @Override
        public void configure(Map<String, ?> map, boolean b) {
        }

        @Override
        public Event deserialize(String s, byte[] data) {
            JsonUtil<Event> utils = new JsonUtil<>();
            String event = new String(data, StandardCharsets.UTF_8);
            return utils.fromJson(event, Event.class);
        }

        @Override
        public void close() {
        }
    }
}
