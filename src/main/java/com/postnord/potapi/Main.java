package com.postnord.potapi;

import com.postnord.potapi.processor.Processor;
import com.postnord.potapi.version.VersionHttpServer;

public class Main {
    private static VersionHttpServer versionServer;

    public static void main(String... args) {
        int port = getPort(args);

        startVersionServer(port);
        startProcessing();
    }

    private static void startProcessing() {
        Processor processor = new Processor();
        processor.process();
    }

    private static int getPort(String... args) {
        int defaultPort = 8080;
        if (args.length > 0) {
            String port = args[0];
            try {
                return Integer.parseInt(port);
            } catch (NumberFormatException e) {
                return defaultPort;
            }
        }

        return defaultPort;
    }

    private static void startVersionServer(int port) {
        versionServer = new VersionHttpServer(port);
        versionServer.start();
    }

    static void shutdown() {
        versionServer.stop();
    }
}
