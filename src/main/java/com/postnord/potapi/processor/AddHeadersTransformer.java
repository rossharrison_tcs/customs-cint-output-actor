package com.postnord.potapi.processor;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;

import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Transaction;

public class AddHeadersTransformer implements Transformer<String, Transaction, KeyValue<String, Transaction>> {

	private ProcessorContext processorContext;

	@Override
	public void init(ProcessorContext processorContext) {
		this.processorContext = processorContext;
	}

	@Override
	public KeyValue<String, Transaction> transform(String key, Transaction value) {
		byte[] itemId = getItemID(value);
		//header as a parcel-ID
		processorContext.headers().add("Parcel-id", itemId);
		return KeyValue.pair(key, value);
	}

	private byte[] getItemID(Transaction value) {
		if (null != value && null != value.getPayload() && !value.getPayload().getItems().isEmpty()) {
			List<Item> items = value.getPayload().getItems();
			if (null != items.get(0).getPnItemId() && StringUtils.isNotEmpty(items.get(0).getPnItemId().getPnItemId()))
				return items.get(0).getPnItemId().getPnItemId().getBytes();
		}
		return new byte[0];
	}

	@Override
	public void close() {
		//Closing Context
	}

}
