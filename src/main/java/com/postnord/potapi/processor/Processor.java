package com.postnord.potapi.processor;

import com.postnord.orm.commons.serialization.transaction.TransactionSerde;
import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.processor.internal.OrmToPotapiMapper;
import com.postnord.potapi.processor.internal.TransactionContainsGLPSource;
import com.postnord.potapi.processor.internal.TransactionToGLPXMLMapper;
import com.postnord.potapi.rule.service.filter.GLPSourceSystemFilter;
import com.postnord.potapi.util.KafkaUtil;
import com.postnord.potapi.util.SystemUtil;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;

import java.util.Properties;

public class Processor {
	private static final String APPLICATION_ID = "customs-cint-output-actor";

	public void process() {
		KafkaStreams kafkaStreams = new KafkaStreams(topology(), config());
		kafkaStreams.start();
	}

	private Properties config() {
		return KafkaUtil.getStreamsApplicationProperties(APPLICATION_ID);
	}

	public Topology topology() {
		
		final Serde<String> stringSerde = Serdes.String();
		
		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, Transaction> potaPi = builder.stream(inputTopic(), consumeTransactionResult())
				 .filter(isNull())
                 .filter(sourceSystemFilter())
                 .filter(currentStateValidation())
                 .filter(applyPrepaidFilter())
	             .mapValues(ormToPotapi())
	             .filter(applyGLPSourceSystemFilter())
	             //addition of header
	             .transform(() -> new AddHeadersTransformer());
		
		potaPi.map(transactionToXMLString())
		      .to(cintOutputTopic(), Produced.with(stringSerde, stringSerde));

		return builder.build();
	}

    private Predicate<String, TransactionResult> applyPrepaidFilter(){
        return (key, transactionResult) -> !(transactionResult.getCurrentState().get().getItems() != null &&
                        !transactionResult.getCurrentState().get().getItems().isEmpty() &&
                        transactionResult.getCurrentState().get().getItems().get(0).getCustoms() != null &&
                transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getOriginals() != null &&
                !transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getOriginals().isEmpty() &&
                transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getOriginals().get(0).getVatPrepaid().isPresent() &&
                transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getOriginals().get(0).getVatPrepaid().get().getValue() != 0 &&
				!transactionResult.getCurrentState().get().getItems().get(0).getAdditionalServiceCodes().isEmpty() &&
				!(transactionResult.getCurrentState().get().getItems().get(0).getAdditionalServiceCodes().contains("CUSTOMS") ||
						transactionResult.getCurrentState().get().getItems().get(0).getAdditionalServiceCodes().contains("RESTRICTED"))
		);
    }

	private Predicate<String, Transaction> applyGLPSourceSystemFilter() {
		GLPSourceSystemFilter glpSourceSystemFilter = new GLPSourceSystemFilter();
		return new TransactionContainsGLPSource(glpSourceSystemFilter);
	}

	private String cintOutputTopic() {
		return SystemUtil.getEnvironmentVariable("CINT_OUTPUT_TOPIC", "prod-glp-se-customs-address-write");
	}

	private KeyValueMapper<String, Transaction, KeyValue<String, String>> transactionToXMLString() {
		return new TransactionToGLPXMLMapper();
	}

	private ValueMapper<TransactionResult, Transaction> ormToPotapi() {
		return new OrmToPotapiMapper();
	}

	private Predicate<String,TransactionResult> isNull() {
		return (k , v ) -> v != null;
	}

	private Predicate<String, TransactionResult> sourceSystemFilter() {
		return (key,
				transactionResult) -> ((
				transactionResult.getTransaction().getBundle().getItems() != null &&
						!transactionResult.getTransaction().getBundle().getItems().isEmpty() &&
						transactionResult.getCurrentState().get().getItems() != null &&
						!transactionResult.getCurrentState().get().getItems().isEmpty() &&
						transactionResult.getTransaction().getBundle().getItems().get(0).getSource().equalsIgnoreCase("POTAPI/ASTA") &&
						transactionResult.getCurrentState().get().getItems().get(0).getCustoms().isPresent() &&
						transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getIsRestrictionChecked().isPresent() &&
						transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getIsRestrictionChecked().get().isChecked()

		) || (
				transactionResult.getCurrentState().get().getItems() != null &&
						!transactionResult.getCurrentState().get().getItems().isEmpty() &&
						transactionResult.getCurrentState().get().getOrders() != null &&
						!transactionResult.getCurrentState().get().getOrders().isEmpty() &&
						transactionResult.getCurrentState().get().getItems().get(0).getCustoms().isPresent() &&
						transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getIsRestrictionChecked().isPresent() &&
						transactionResult.getCurrentState().get().getItems().get(0).getCustoms().get().getIsRestrictionChecked().get().isChecked() &&
						transactionResult.getCurrentState().get().getOrders().get(0).getSource().equalsIgnoreCase("POTAPI/GLP")
		));
		/*return (key,
				transactionResult) -> ((
						transactionResult.getTransaction().getBundle().getItems() != null &&
						!transactionResult.getTransaction().getBundle().getItems().isEmpty() &&
                                transactionResult.getCurrentState().get().getItems() != null &&
                                !transactionResult.getCurrentState().get().getItems().isEmpty() &&
						transactionResult.getCurrentState().get().getOrders() != null &&
						!transactionResult.getCurrentState().get().getOrders().isEmpty() &&
						transactionResult.getTransaction().getBundle().getItems().get(0).getSource().equalsIgnoreCase("POTAPI/ASTA") &&
								transactionResult.getCurrentState().get().getItems().get(0).getSource().equalsIgnoreCase("POTAPI/ASTA_CUSTOMS_OUTPUT") &&
								(transactionResult.getCurrentState().get().getOrders().get(0).getSource().equalsIgnoreCase("POP")
								|| transactionResult.getCurrentState().get().getOrders().get(0).getSource().equalsIgnoreCase("POR"))
		) || (
				transactionResult.getTransaction().getBundle().getItems() != null &&
						!transactionResult.getTransaction().getBundle().getItems().isEmpty() &&
						transactionResult.getCurrentState().get().getOrders() != null &&
						!transactionResult.getCurrentState().get().getOrders().isEmpty() &&
						transactionResult.getTransaction().getBundle().getItems().get(0).getSource().equalsIgnoreCase("POTAPI/ASTA_CUSTOMS_OUTPUT") &&
						transactionResult.getCurrentState().get().getOrders().get(0).getSource().equalsIgnoreCase("POTAPI/GLP")
				));*/
	}

    private Predicate<String, TransactionResult> currentStateValidation() {
        return (key,
                transactionResult) -> (null != transactionResult.getCurrentState().get().getOrders() && transactionResult.getCurrentState().get().getOrders().size() > 0);
    }

	private Consumed<String, TransactionResult> consumeTransactionResult() {
		Serde<String> keySerde = Serdes.String();
        Serde<TransactionResult> valueSerde = TransactionSerde.transactionResult();
		return Consumed.with(keySerde, valueSerde);
	}

	private String inputTopic() {
		return SystemUtil.getEnvironmentVariable("INPUT_TOPIC", "transaction-commit");
	}
}
