package com.postnord.potapi.processor.internal;

import com.postnord.orm.domain.*;
import com.postnord.orm.service.transaction.model.Action;
import com.postnord.orm.service.transaction.model.Actor;
import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.Address;
import com.postnord.potapi.model.types.*;
import com.postnord.potapi.model.types.Party;
import com.postnord.potapi.model.types.PartyAddress;
import org.apache.kafka.streams.kstream.ValueMapper;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class OrmToPotapiMapper implements ValueMapper<TransactionResult, Transaction> {

    @Override
    public Transaction apply(TransactionResult value) {
        Transaction.Builder transactionBuilder = new Transaction.Builder();

        com.postnord.orm.service.transaction.model.Transaction ormTransaction = value.getTransaction();
        UUID transactionId = UUID.randomUUID();
        transactionBuilder.withId(transactionId.toString());

        int retries = ormTransaction.getRetries();
        transactionBuilder.withRetries(retries);

        Action action = ormTransaction.getAction();
        transactionBuilder.withAction(action.getLabel());

        Actor actor = ormTransaction.getActor();
        transactionBuilder.withActor(actor.name());

        OffsetDateTime processTime = ormTransaction.getProcessTime();
        String processTimeString = processTime.toString();
        transactionBuilder.withProcessingTime(processTimeString);

        String eventTime = getEventTime(ormTransaction);
        transactionBuilder.withEventTime(eventTime);

        Payload payload = extractPayload(value);
        transactionBuilder.withPayload(payload);

        return transactionBuilder.build();
    }

    private String getEventTime(com.postnord.orm.service.transaction.model.Transaction ormTransaction) {
        DataBundle bundle = ormTransaction.getBundle();
        List<Order> orders = bundle.getOrders();

        if (orders.isEmpty()) {
            return null;
        }

        for (Order order : orders) {
            Optional<OffsetDateTime> candidate = order.getEventTime();
            if (candidate.isPresent()) {
                OffsetDateTime offsetDateTime = candidate.get();
                return offsetDateTime.toString();
            }
        }

        return null;
    }

    private Payload extractPayload(TransactionResult value) {
        Payload.Builder builder = new Payload.Builder();

        if(value.getCurrentState() != null && !value.getCurrentState().get().getItems().isEmpty()
                && !value.getCurrentState().get().getOrders().isEmpty()){

            List<Order> orders =value.getCurrentState().get().getOrders();
            addOrders(orders, builder);

            List<Item> items = value.getCurrentState().get().getItems();
            addItems(items, builder);
        }

        return builder.build();
    }

    private void addOrders(List<Order> ormOrders, Payload.Builder potapiBuilder) {
        for (Order ormOrder : ormOrders) {

            com.postnord.potapi.model.Order.Builder orderBuilder = new com.postnord.potapi.model.Order.Builder();

            String orderId = ormOrder.getOrderId();
            orderBuilder.withOrderId(orderId);

            Optional<String> externalId = ormOrder.getExternalId();
            externalId.ifPresent(orderBuilder::withExternalId);

            String source = ormOrder.getSource();
            orderBuilder.withSource(source);


            OffsetDateTime processingTime = ormOrder.getProcessingTime();
            if (processingTime != null) {
                orderBuilder.withProcessingTime(processingTime.toString());
            }


            Integer currentVersion = ormOrder.getCurrentVersion();
            orderBuilder.withCurrentVersion(currentVersion);


            Integer baseVersion = ormOrder.getBaseVersion();
            orderBuilder.withBaseVersion(baseVersion);

            String transactionId = ormOrder.getTransactionId();
            orderBuilder.withTransactionId(transactionId);


            Optional<String> businessTransactionId = ormOrder.getBusinessTransactionId();
            businessTransactionId.ifPresent(orderBuilder::withBusinessTransactionId);


            Optional<String> pnShipmentId = ormOrder.getPnShipmentId();
            pnShipmentId.ifPresent(orderBuilder::withPnShipmentId);


            List<String> itemIds = ormOrder.getItemIds();
            for (String itemId : itemIds) {
                orderBuilder.withItemId(itemId);
            }


            Optional<Integer> shipmentAmount = ormOrder.getShipmentAmount();
            shipmentAmount.ifPresent(orderBuilder::withShipmentAmount);


            addConsignorPartyInfo(ormOrder, orderBuilder);
            addConsigneePartyInfo(ormOrder, orderBuilder);


            withPickupTimeWindow(ormOrder, orderBuilder);
            withDeliveryTimeWindow(ormOrder, orderBuilder);

            com.postnord.potapi.model.Order order = orderBuilder.build();

            potapiBuilder.withOrder(order);
        }
    }

    private void addConsignorPartyInfo(Order ormOrder, com.postnord.potapi.model.Order.Builder orderBuilder) {
        PartyOriginal.Builder partyOriginalBuilder = new PartyOriginal.Builder();

        //Optional<String> addressee = ormOrder.getConsignerName();
        //addressee.ifPresent(partyOriginalBuilder::withAddressee);

        Optional<String> ConsignerName = ormOrder.getConsignerName();
        ConsignerName.ifPresent(partyOriginalBuilder::withName);

        Optional<ContactInfo> consignorContactInfo = ormOrder.getConsignerContactInfo();

        if (consignorContactInfo.isPresent()) {
            ContactInfo contactInfo = consignorContactInfo.get();

            Optional<String> name = contactInfo.getName();
            name.ifPresent(partyOriginalBuilder::withName);

            Optional<String> phone = contactInfo.getPhone();
            phone.ifPresent(partyOriginalBuilder::withPhone);

            Optional<String> email = contactInfo.getEmail();
            email.ifPresent(partyOriginalBuilder::withEmail);
        }


        Optional<EdiAddress> consignerText = ormOrder.getConsignerText();
        if (consignerText.isPresent()) {
            EdiAddress ediAddress = consignerText.get();

            Optional<String> consignorAddressee = ediAddress.getAddressee();
            consignorAddressee.ifPresent(partyOriginalBuilder::withAddressee);

            String addressRow = ediAddress.getAddressRow1();
            Optional<String> addressRow2 = ediAddress.getAddressRow2();
            if (addressRow2.isPresent()) {
                addressRow += "\n" + addressRow2.get();
            }
            partyOriginalBuilder.withAddressRow(addressRow);

            Optional<String> postalCode = ediAddress.getPostalCode();
            postalCode.ifPresent(partyOriginalBuilder::withPostalCode);


            Optional<String> city = ediAddress.getCity();
            city.ifPresent(partyOriginalBuilder::withCity);


            String country = ediAddress.getCountry();
            partyOriginalBuilder.withCountry(country);
        }


        PartyOriginal partyOriginal = partyOriginalBuilder
                .build();


        Address.Builder addressBuilder = new Address.Builder();

        Optional<String> consignerAddressId = ormOrder.getConsignerAddressId();
        consignerAddressId.ifPresent(addressBuilder::withAddressId);

        Optional<Float> consignerIdAccuracy = ormOrder.getConsignerIdAccuracy();
        if (consignerIdAccuracy.isPresent()) {
            Float accuracy = consignerIdAccuracy.get();
            addressBuilder.withMatchAccuracy(accuracy.doubleValue());
        }

        Optional<AddressInfo> consignerAddressInfo = ormOrder.getConsignerAddressInfo();
        if (consignerAddressInfo.isPresent()) {
            AddressInfo addressInfo = consignerAddressInfo.get();

            String streetName = addressInfo.getStreetName();
            addressBuilder.withStreetName(streetName);

            Optional<String> addressNumber = addressInfo.getAddressNumber();
            addressNumber.ifPresent(addressBuilder::withAddressNumber);

            Optional<String> addressNumberSuffix = addressInfo.getAddressNumberSuffix();
            addressNumberSuffix.ifPresent(addressBuilder::withAddressNumberSuffix);

            String postalCode = addressInfo.getPostalCode();
            addressBuilder.withPostalCode(postalCode);

            Optional<String> city = addressInfo.getCity();
            city.ifPresent(addressBuilder::withCity);

            String country = addressInfo.getCountry();
            addressBuilder.withCountry(country);

            Optional<String> stairwayIdentifier = addressInfo.getStairwayIdentifier();
            stairwayIdentifier.ifPresent(addressBuilder::withStairwayIdentifier);

            Optional<String> floorIdentifier = addressInfo.getFloorIdentifier();
            floorIdentifier.ifPresent(addressBuilder::withFloorIdentifier);

            Optional<String> doorIdentifier = addressInfo.getDoorIdentifier();
            doorIdentifier.ifPresent(addressBuilder::withDoorIdentifier);

            Optional<String> buildingNumber = addressInfo.getBuildingNumber();
            buildingNumber.ifPresent(addressBuilder::withBuildingNumber);
        }

        Address address = addressBuilder
                .build();


        Party.Builder partyEnrichedBuilder = new Party.Builder();

        Optional<String> consignorId = ormOrder.getConsignerId();
        consignorId.ifPresent(partyEnrichedBuilder::withId);

        Optional<String> consignerName = ormOrder.getConsignerName();
        consignerName.ifPresent(partyEnrichedBuilder::withName);


        Party party = partyEnrichedBuilder
                .build();


        PartyAddress.Builder partyAddressbuilder = new PartyAddress.Builder();


        PartyAddress partyAddress = partyAddressbuilder
                .build();


        PartyEnriched partyInfoEnriched = new PartyEnriched.Builder()
                .withAddress(address)
                .withParty(party)
                .withPartyAddress(partyAddress)
                .build();


        PartyInfo consignorPartyInfo = new PartyInfo.Builder()
                .withOriginal(partyOriginal)
                .withEnriched(partyInfoEnriched)
                .build();

        orderBuilder.withConsignorPartyInfo(consignorPartyInfo);
    }

    private void addConsigneePartyInfo(Order ormOrder, com.postnord.potapi.model.Order.Builder orderBuilder) {
        PartyOriginal.Builder partyOriginalBuilder = new PartyOriginal.Builder();

        //Optional<String> addressee = ormOrder.getConsigneeName();
        //addressee.ifPresent(partyOriginalBuilder::withAddressee);

        Optional<String> consigneeName = ormOrder.getConsigneeName();
        consigneeName.ifPresent(partyOriginalBuilder::withName);

        Optional<ContactInfo> consigneeContactInfo = ormOrder.getConsigneeContactInfo();

        if (consigneeContactInfo.isPresent()) {
            ContactInfo contactInfo = consigneeContactInfo.get();

            Optional<String> name = contactInfo.getName();
            name.ifPresent(partyOriginalBuilder::withName);

            Optional<String> phone = contactInfo.getPhone();
            phone.ifPresent(partyOriginalBuilder::withPhone);

            Optional<String> email = contactInfo.getEmail();
            email.ifPresent(partyOriginalBuilder::withEmail);
        }


        Optional<EdiAddress> consigneeText = ormOrder.getConsigneeText();
        if (consigneeText.isPresent()) {
            EdiAddress ediAddress = consigneeText.get();

            Optional<String> consigneeAddressee = ediAddress.getAddressee();
            consigneeAddressee.ifPresent(partyOriginalBuilder::withAddressee);

            String addressRow = ediAddress.getAddressRow1();
            Optional<String> addressRow2 = ediAddress.getAddressRow2();
            if (addressRow2.isPresent()) {
                addressRow += "\n" + addressRow2.get();
            }
            partyOriginalBuilder.withAddressRow(addressRow);

            Optional<String> postalCode = ediAddress.getPostalCode();
            postalCode.ifPresent(partyOriginalBuilder::withPostalCode);

            Optional<String> city = ediAddress.getCity();
            city.ifPresent(partyOriginalBuilder::withCity);

            String country = ediAddress.getCountry();
            partyOriginalBuilder.withCountry(country);
        }

        PartyOriginal original = partyOriginalBuilder
                .build();


        Address.Builder addressBuilder = new Address.Builder();

        Optional<String> consigneeAddressId = ormOrder.getConsigneeAddressId();
        consigneeAddressId.ifPresent(addressBuilder::withAddressId);

        Optional<Float> consigneeIdAccuracy = ormOrder.getConsigneeIdAccuracy();
        if (consigneeIdAccuracy.isPresent()) {
            Float accuracy = consigneeIdAccuracy.get();
            addressBuilder.withMatchAccuracy(accuracy.doubleValue());
        }

        Optional<AddressInfo> consigneeAddressInfo = ormOrder.getConsigneeAddressInfo();
        if (consigneeAddressInfo.isPresent()) {
            AddressInfo addressInfo = consigneeAddressInfo.get();

            String streetName = addressInfo.getStreetName();
            addressBuilder.withStreetName(streetName);

            Optional<String> addressNumber = addressInfo.getAddressNumber();
            addressNumber.ifPresent(addressBuilder::withAddressNumber);

            Optional<String> addressNumberSuffix = addressInfo.getAddressNumberSuffix();
            addressNumberSuffix.ifPresent(addressBuilder::withAddressNumberSuffix);

            String postalCode = addressInfo.getPostalCode();
            addressBuilder.withPostalCode(postalCode);

            Optional<String> city = addressInfo.getCity();
            city.ifPresent(addressBuilder::withCity);

            String country = addressInfo.getCountry();
            addressBuilder.withCountry(country);

            Optional<String> stairwayIdentifier = addressInfo.getStairwayIdentifier();
            stairwayIdentifier.ifPresent(addressBuilder::withStairwayIdentifier);

            Optional<String> floorIdentifier = addressInfo.getFloorIdentifier();
            floorIdentifier.ifPresent(addressBuilder::withFloorIdentifier);

            Optional<String> doorIdentifier = addressInfo.getDoorIdentifier();
            doorIdentifier.ifPresent(addressBuilder::withDoorIdentifier);

            Optional<String> buildingNumber = addressInfo.getBuildingNumber();
            buildingNumber.ifPresent(addressBuilder::withBuildingNumber);
        }

        Address address = addressBuilder
                .build();


        Party.Builder partyEnrichedBuilder = new Party.Builder();

        Optional<String> consigneeId = ormOrder.getConsigneeId();
        consigneeId.ifPresent(partyEnrichedBuilder::withId);

        Optional<String> consigneeNameEnrich = ormOrder.getConsigneeName();
        consigneeNameEnrich.ifPresent(partyEnrichedBuilder::withName);

        Party party = partyEnrichedBuilder
                .build();


        PartyAddress.Builder partyAddressbuilder = new PartyAddress.Builder();


        PartyAddress partyAddress = partyAddressbuilder
                .build();


        PartyEnriched enriched = new PartyEnriched.Builder()
                .withAddress(address)
                .withParty(party)
                .withPartyAddress(partyAddress)
                .build();

        PartyInfo consignee = new PartyInfo.Builder()
                .withOriginal(original)
                .withEnriched(enriched)
                .build();

        orderBuilder.withConsigneePartyInfo(consignee);
    }

    private void withPickupTimeWindow(Order ormOrder, com.postnord.potapi.model.Order.Builder orderBuilder) {
        Optional<TimeInterval> ormPickupTimeWindow = ormOrder.getPickupTimeWindow();

        if (ormPickupTimeWindow.isPresent()) {
            TimeInterval ormTimeInterval = ormPickupTimeWindow.get();

            com.postnord.potapi.model.TimeInterval.Builder builder = new com.postnord.potapi.model.TimeInterval.Builder();

            OffsetDateTime begin = ormTimeInterval.getBegin();
            builder.withBegin(begin.toString());

            OffsetDateTime end = ormTimeInterval.getEnd();
            builder.withEnd(end.toString());

            com.postnord.potapi.model.TimeInterval pickupTimeWindow = builder.build();
            orderBuilder.withPickupTimeWindow(pickupTimeWindow);
        }
    }

    private void withDeliveryTimeWindow(Order ormOrder, com.postnord.potapi.model.Order.Builder orderBuilder) {
        Optional<TimeInterval> ormDeliveryTimeWindow = ormOrder.getDeliveryTimeWindow();

        if (ormDeliveryTimeWindow.isPresent()) {
            TimeInterval ormTimeInterval = ormDeliveryTimeWindow.get();

            com.postnord.potapi.model.TimeInterval.Builder builder = new com.postnord.potapi.model.TimeInterval.Builder();

            OffsetDateTime begin = ormTimeInterval.getBegin();
            builder.withBegin(begin.toString());

            OffsetDateTime end = ormTimeInterval.getEnd();
            builder.withEnd(end.toString());

            com.postnord.potapi.model.TimeInterval deliveryTimeWindow = builder.build();
            orderBuilder.withDeliveryTimeWindow(deliveryTimeWindow);
        }
    }

    private void addItems(List<Item> ormItems, Payload.Builder potapiBuilder) {
        for (Item ormItem : ormItems) {
            com.postnord.potapi.model.Item.Builder itemBuilder = new com.postnord.potapi.model.Item.Builder();


            Optional<String> pnItemId = ormItem.getPnItemId();
            pnItemId.ifPresent(itemBuilder::withPnItemId);


            String itemId = ormItem.getItemId();
            itemBuilder.withItemId(itemId);


            Integer currentVersion = ormItem.getCurrentVersion();
            itemBuilder.withCurrentVersion(currentVersion);

            Integer baseVersion = ormItem.getBaseVersion();
            itemBuilder.withBaseVersion(baseVersion);
            ormItem.getCustoms();
            itemBuilder.withCustoms(buildOriginals(ormItem));


            String productCode = ormItem.getProductCode();
            itemBuilder.withProductCode(productCode);


            Optional<String> itemContainerId = ormItem.getItemContainerId();
            itemContainerId.ifPresent(itemBuilder::withItemContainerId);


            String source = ormItem.getSource();
            itemBuilder.withSource(source);


            OffsetDateTime processingTime = ormItem.getProcessingTime();
            if (processingTime != null) {
                itemBuilder.withProcessingTime(processingTime.toString());
            }


            String transactionId = ormItem.getTransactionId();
            itemBuilder.withTransactionId(transactionId);


            List<String> serviceCodes = ormItem.getAdditionalServiceCodes();
            for (String serviceCode : serviceCodes) {
                itemBuilder.withServiceCode(serviceCode);
            }


            List<String> barCodes = ormItem.getBarcodes();
            for (String barCode : barCodes) {
                itemBuilder.withBarCode(barCode);
            }


            Optional<String> itemState = ormItem.getItemState();
            itemState.ifPresent(itemBuilder::withItemState);


            Optional<String> externalId = ormItem.getExternalId();
            externalId.ifPresent(itemBuilder::withExternalId);


            Optional<String> orderId = ormItem.getOrderId();
            orderId.ifPresent(itemBuilder::withOrderId);


            Optional<Dimensions> dimension = ormItem.getDimensions();
            dimension.ifPresent(d -> {
                Dimension dim = new Dimension.Builder()
                        .withDepth(d.getDepth())
                        .withHeight(d.getHeight())
                        .withWidth(d.getWidth())
                        .build();

                itemBuilder.withDimension(dim);
            });

            Optional<Integer> weight = ormItem.getWeight();
            weight.ifPresent(itemBuilder::withWeight);


            com.postnord.potapi.model.Item item = itemBuilder.build();
            potapiBuilder.withItem(item);
        }
    }

    private com.postnord.potapi.model.types.Customs buildOriginals(Item ormItem) {
        com.postnord.potapi.model.types.Customs.Builder customsBuilder = new com.postnord.potapi.model.types.Customs.Builder();
        Optional<com.postnord.orm.domain.Customs> customs = ormItem.getCustoms();
        List<com.postnord.orm.domain.CustomsOriginal> originals=null;
        if(customs.isPresent()) {
            com.postnord.orm.domain.Customs original = customs.get();
            originals =  original.getOriginals();
        }
        customsBuilder.withOriginal(buildCustomsOriginals(originals));
        return customsBuilder.build();

    }

    private com.postnord.potapi.model.types.CustomsOriginal buildCustomsOriginals(List<com.postnord.orm.domain.CustomsOriginal> originals) {
        com.postnord.potapi.model.types.CustomsOriginal.Builder customsOriginals = new com.postnord.potapi.model.types.CustomsOriginal.Builder();
        com.postnord.orm.domain.CustomsOriginal original = null;
        if(originals.size() > 0) {
            original = originals.get(0);
            Optional<com.postnord.orm.domain.GoodsCategory> goodsCategory = original.getGoodsCategory();
            goodsCategory.ifPresent(d ->{
                customsOriginals.withGoodsCategory(d != null ? d.getGoodscategory() : null);
            });
            Optional<com.postnord.orm.domain.GoodsDescription> goodsDescription = original.getGoodsDescription();
            goodsDescription.ifPresent(d ->{
                customsOriginals.withGoodsDescription(d != null ? d.getGoodsdescription() : null);
            });

            Optional<com.postnord.orm.domain.HsTariffNumber> hsTeriffNumber = original.getHsTariffNumber();
            hsTeriffNumber.ifPresent(d ->{
                customsOriginals.withHsTariffNumber(d != null ? d.getHstariffnumber() : null);
            });
            Optional<com.postnord.orm.domain.IsCommercialRecipient> receipient = original.getIsCommercialRecipient();
            receipient.ifPresent(d ->{
                customsOriginals.withIsCommercialRecipient(d.isGift());
            });

            Optional<com.postnord.orm.domain.IsDocument> isDocument = original.getIsDocument();
            isDocument.ifPresent(d ->{
                customsOriginals.withIsDocument(d.isDocument());
            });

            Optional<com.postnord.orm.domain.IsGift> isGift = original.getIsGift();
            isGift.ifPresent(d ->{
                customsOriginals.withIsGift(d.isGift());
            });

            Optional<com.postnord.orm.domain.IsSample> isSample = original.getIsSample();
            isSample.ifPresent(d ->{
                customsOriginals.withIsSample(d.isSample());
            });

            //Added for EU_flag
            Optional<com.postnord.orm.domain.PnImportCategory> pnImportCategory = original.getPnImportCategory();
            pnImportCategory.ifPresent(d ->{
                customsOriginals.withPnImportCategory(d.getPnimportcategory());
            });

            try {
                Optional<com.postnord.orm.domain.Money> money = original.getGoodsValue();
                money.ifPresent(d -> {
                    customsOriginals.withGoodsValue(d.getValue().doubleValue(), d.getCurrency().getCurrencycode().get().toString());
                });

                Optional<com.postnord.orm.domain.Money> PreVat = original.getVatPrepaid();
                PreVat.ifPresent(d -> {
                    customsOriginals.withVatPrepaid(d.getValue().doubleValue(), d.getCurrency().getCurrencycode().get().toString());
                });
            }catch(IllegalArgumentException e){

            }

        }

        return customsOriginals.build();
    }
}