package com.postnord.potapi.processor.internal;

import org.apache.kafka.streams.kstream.Predicate;

import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.rule.service.filter.GLPSourceSystemFilter;

public class TransactionContainsGLPSource implements Predicate<String, Transaction> {

	private GLPSourceSystemFilter gLPSourceSystemFilter;

	public TransactionContainsGLPSource(GLPSourceSystemFilter gLPSourceSystemFilter) {
		this.gLPSourceSystemFilter = gLPSourceSystemFilter;
	}

	@Override
	public boolean test(String key, Transaction transaction) {
		return gLPSourceSystemFilter.sourceSystemFilter(transaction);
	}

}
