package com.postnord.potapi.processor.internal;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.util.TransactionToGLPXMLUtil;

public class TransactionToGLPXMLMapper implements KeyValueMapper<String, Transaction, KeyValue<String, String>>{

	@Override
	public KeyValue<String, String> apply(String k, Transaction transaction) {
		String key = transaction.getTransactionId().getTransactionId();
		TransactionToGLPXMLUtil transactionToGLPXMLUtil = new TransactionToGLPXMLUtil();
		String glpXMLString = transactionToGLPXMLUtil.getGLPXMLString(transaction);
		return new KeyValue<>(key, glpXMLString);
	}
}
