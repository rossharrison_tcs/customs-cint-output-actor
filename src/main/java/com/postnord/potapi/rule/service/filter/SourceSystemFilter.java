package com.postnord.potapi.rule.service.filter;

import com.postnord.potapi.model.Transaction;

public interface SourceSystemFilter {
	boolean sourceSystemFilter(Transaction transaction);
}
