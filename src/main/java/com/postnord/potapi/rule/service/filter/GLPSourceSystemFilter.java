package com.postnord.potapi.rule.service.filter;

import com.postnord.potapi.model.Transaction;

public class GLPSourceSystemFilter implements SourceSystemFilter {

	@Override
	public boolean sourceSystemFilter(Transaction transaction) {
		return transaction.getActor().getActor().equalsIgnoreCase("POTAPI")
				&& transaction.getAction().getAction().equalsIgnoreCase("WRITE")
				&& transaction.getRetries().getRetries().intValue() == 0;
	}

}
