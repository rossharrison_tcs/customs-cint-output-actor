package com.postnord.potapi.util;



import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import com.postnord.potapi.model.*;
import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Order;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.CustomsOriginal;
import com.postnord.potapi.model.types.PartyInfo;
import org.apache.commons.lang3.StringUtils;

public class TransactionToGLPXMLUtil {
	
	public String getGLPXMLString(Transaction transaction) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Event.class);
			return createGKLXMLString(getGLPEvent(transaction),jaxbContext);
		} catch (JAXBException e) {
			return null;
		}
	}
	
	private String createGKLXMLString(Event event, JAXBContext jaxbContext) throws JAXBException {
		JAXBIntrospector introspector = jaxbContext.createJAXBIntrospector();
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        StringWriter sw = new StringWriter();
       //marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
        
        if(null == introspector.getElementName(event)) {
            JAXBElement jaxbElement = new JAXBElement(new QName("","ns2:event"), Event.class, event);
            marshaller.marshal(jaxbElement, sw);
        } else {
            marshaller.marshal(event, sw);
        }
                
        return sw.toString();
    }

	private Event getGLPEvent(Transaction transaction) {
		ObjectFactory objectFactory = new ObjectFactory();
		Event event = objectFactory.createEvent();

		event.setSchemaVersion(BigDecimal.valueOf(1.1));
		event.setUserId("IC1");

		//Adding localCode hardcoding as mandatory in CINT
		LocalCodeType localCode = objectFactory.createLocalCodeType();
		localCode.setType("event");
		if(transaction.getPayload() != null && transaction.getPayload().getItems() != null){
			com.postnord.potapi.model.Item item = transaction.getPayload().getItems().get(0);
			if(item.getCustoms() != null && item.getCustoms().getOriginals() != null){
				com.postnord.potapi.model.types.CustomsOriginal originals = item.getCustoms().getOriginals().get(0);
				localCode.setValue(originals.getHsTariffNumber() != null
						&& originals.getHsTariffNumber().getHsTariffNumber() != null ? originals.getHsTariffNumber().getHsTariffNumber() : "116");
			}else{
				localCode.setValue("116");
			}
		}else{
			localCode.setValue("116");
		}
		//localCode.setValue("116");
		event.setLocalCode(localCode);

		//Adding location hardcoding as mandatory in CINT
		LocationType location = objectFactory.createLocationType();
		location.setId("1022475");
		location.setName("ARLANDA BREVTERMINAL");
		location.setType("IPMC");
		event.setLocation(location);

		if (null != transaction.getPayload() && null != transaction.getPayload().getItems()
				&& !transaction.getPayload().getItems().isEmpty()) {
			CustomsLabelType customsLabel = objectFactory.createCustomsLabelType();
			Payload payload = transaction.getPayload();
			List<Item> items = payload.getItems();
			Item item = items.get(0);
			if (item != null) {
				event.setDate(item.getProcessingTime() != null ? item.getProcessingTime().getProcessingTime().toString() : null);
				event.setId(item.getPnItemId().getPnItemId());
				ServiceType serviceType = objectFactory.createServiceType();
				if(null != item.getProductCode() && null != item.getProductCode().getProductCode()) {
					serviceType.setCode(item.getProductCode().getProductCode());
				}
				event.setService(serviceType);
				AdditionalServiceType additionalServiceType = objectFactory.createAdditionalServiceType();
				if (null != item.getServiceCodes() && !item.getServiceCodes().isEmpty()) {
					if(item.getServiceCodes().contains("CUSTOMS")){
						additionalServiceType.setCode("CUSTOMS");
					}else if(item.getServiceCodes().contains("GIFT")){
						additionalServiceType.setCode("GIFT");
					}else if(item.getServiceCodes().contains("VAT")){
						additionalServiceType.setCode("VAT");
					}else{
						additionalServiceType.setCode(item.getServiceCodes().get(0).getServiceCode());
					}
				}
				event.setAdditionalService(additionalServiceType);
				WeightType weightType = objectFactory.createWeightType();
				if(null != item.getWeight() && null != item.getWeight().getWeight()){
					weightType.setUnit("g");
                    weightType.setValue(BigDecimal.valueOf(item.getWeight().getWeight()));
					customsLabel.setTotalStatedWeight(weightType);
                }

				if (null != item.getCustoms() && null != item.getCustoms().getOriginals()) {
					CustomsOriginal customsOriginal = item.getCustoms().getOriginals().get(0);
					if(null != customsOriginal.getCommercialRecipient())
						customsLabel.setIsCommercialRecipient(customsOriginal.getCommercialRecipient().getGift());
					if(null != customsOriginal.getIsGift())
						customsLabel.setIsGift(customsOriginal.getIsGift().getGift());
					if(null != customsOriginal.getIsSample())
						customsLabel.setIsCommercialSample(customsOriginal.getIsSample().getSample());
					if(null != customsOriginal.getGoodsDescription())
						customsLabel.setItemDescription(customsOriginal.getGoodsDescription().getGoodsCategory());
                    if(null != customsOriginal.getIsDocument()){
                        customsLabel.setIsDocument(customsOriginal.getIsDocument().getDocument());
                    }
					if (null != customsOriginal.getGoodsValue()) {
						AmountType amountType = objectFactory.createAmountType();
						if(null != customsOriginal.getGoodsValue().getCurrency())
							amountType.setCurrency(customsOriginal.getGoodsValue().getCurrency().getCurrencyCode());
						if(null != customsOriginal.getGoodsValue().getGoodsValue())
							amountType.setValue(BigDecimal.valueOf(customsOriginal.getGoodsValue().getGoodsValue()));
						customsLabel.setTotalStatedValue(amountType);
					}

					//Added for EU_flag
					if(null != customsOriginal.getPnImportCategory()){
						customsLabel.setOriginatingCountry(customsOriginal.getPnImportCategory().getPnImportCategory());
					}
				}
			}
			event.setCustomsLabel(customsLabel);
			if (payload.getOrders() != null && !payload.getOrders().isEmpty()) {
				Order order = payload.getOrders().stream().findFirst().get();
				PartyInfo consigneeInfo = order.getConsignee();
				PartyInfo consignorInfo = order.getConsignor();
				event.setConsignee(getConsigneeInformation(consigneeInfo, objectFactory, event));
				event.setConsignor(getConsignorInformation(consignorInfo, objectFactory, event));
			}

		}
		return event;
	}
	
	
	private ConsignorType getConsignorInformation(PartyInfo partyInfo, ObjectFactory objectFactory, Event event) {
		ConsignorType consignorType = objectFactory.createConsignorType();
		AddressType addressType = objectFactory.createAddressType();
		if (partyInfo != null && partyInfo.getOriginal() != null) {
			if (null != partyInfo.getOriginal().getCountry() && StringUtils.isNotBlank(partyInfo.getOriginal().getCountry().getCountry()) )
				addressType.setCountry(partyInfo.getOriginal().getCountry().getCountry());
			if (null != partyInfo.getOriginal().getCity() && StringUtils.isNotBlank(partyInfo.getOriginal().getCity().getCity()))
				addressType.setCity(partyInfo.getOriginal().getCity().getCity());
			if (null != partyInfo.getOriginal().getPostalCode() && StringUtils.isNotBlank(partyInfo.getOriginal().getPostalCode().getPostalCode()))
				addressType.setPostcode(partyInfo.getOriginal().getPostalCode().getPostalCode());
			if (null != partyInfo.getOriginal().getAddressee() && StringUtils.isNotBlank(partyInfo.getOriginal().getAddressee().getAddressee()))
				addressType.setStreet1(partyInfo.getOriginal().getAddressee().getAddressee());
			if (null != partyInfo.getOriginal().getAddressRow() && StringUtils.isNotBlank(partyInfo.getOriginal().getAddressRow().getAddressRow()))
				addressType.setStreet2(partyInfo.getOriginal().getAddressRow().getAddressRow());

			consignorType.setAddress(addressType);
		}
		return consignorType;
	}

	public ConsigneeType getConsigneeInformation(PartyInfo partyInfo, ObjectFactory objectFactory, Event event) {
		ConsigneeType consigneeType = objectFactory.createConsigneeType();
		AddressType addressType = objectFactory.createAddressType();
		ContactType contactType = objectFactory.createContactType();
		if (partyInfo != null && partyInfo.getOriginal() != null) {
			if (null != partyInfo.getOriginal().getCountry() && StringUtils.isNotBlank(partyInfo.getOriginal().getCountry().getCountry()))
				addressType.setCountry(partyInfo.getOriginal().getCountry().getCountry());
			if (null != partyInfo.getOriginal().getCity() && StringUtils.isNotBlank(partyInfo.getOriginal().getCity().getCity()))
				addressType.setCity(partyInfo.getOriginal().getCity().getCity());
			if (null != partyInfo.getOriginal().getPostalCode() && StringUtils.isNotBlank(partyInfo.getOriginal().getPostalCode().getPostalCode()))
				addressType.setPostcode(partyInfo.getOriginal().getPostalCode().getPostalCode());
			if (null != partyInfo.getOriginal().getAddressee() && StringUtils.isNotBlank(partyInfo.getOriginal().getAddressee().getAddressee()))
				addressType.setStreet1(partyInfo.getOriginal().getAddressee().getAddressee());
			if (null != partyInfo.getOriginal().getAddressRow() && StringUtils.isNotBlank(partyInfo.getOriginal().getAddressRow().getAddressRow()))
				addressType.setStreet2(partyInfo.getOriginal().getAddressRow().getAddressRow());
			consigneeType.setAddress(addressType);
			if (null != partyInfo.getOriginal().getName() && StringUtils.isNotBlank(partyInfo.getOriginal().getName().getName()))
				consigneeType.setName(partyInfo.getOriginal().getName().getName());
	/*		else if (null != partyInfo.getOriginal().getName() && StringUtils.isNotBlank(partyInfo.getOriginal().getName().getName()))
				consigneeType.setName(partyInfo.getOriginal().getName().getName());*/
			if (null != partyInfo.getOriginal().getPhone() && StringUtils.isNotBlank(partyInfo.getOriginal().getPhone().getPhone()))
				contactType.setPhone(partyInfo.getOriginal().getPhone().getPhone());
			consigneeType.setContact(contactType);
		}
		return consigneeType;
	}
}
