package com.postnord.potapi.util;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import java.io.File;
import java.util.Objects;
import java.util.Properties;

public class KafkaUtil {

    public static Properties getStreamsApplicationProperties(String defaultApplicationId) {
        int requestSize = 20 * 1024 * 1024; // 20 megabyte

        Properties config = new Properties();

        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, KafkaUtil.getKafkaBootstrapServers());
        config.put(CommonClientConfigs.CONNECTIONS_MAX_IDLE_MS_CONFIG, 7 * 60 * 1000);

        config.put(StreamsConfig.APPLICATION_ID_CONFIG, getStreamsApplicationId(defaultApplicationId));
        config.put(StreamsConfig.CLIENT_ID_CONFIG, getStreamsClientId(defaultApplicationId));
        config.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, getNumKafkaBrokersInOrmCluster());
        config.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, SystemUtil.getStreamsOffsetReset("STREAMS_NUM_THREADS", 1));
        config.put(StreamsConfig.producerPrefix(ProducerConfig.ACKS_CONFIG), "1");
        config.put(StreamsConfig.consumerPrefix(ConsumerConfig.MAX_POLL_RECORDS_CONFIG), SystemUtil.getStreamsOffsetReset("STREAMS_MAX_POLL_RECORDS", 1000));
        config.put(StreamsConfig.producerPrefix(ProducerConfig.MAX_REQUEST_SIZE_CONFIG), requestSize);
        config.put(StreamsConfig.consumerPrefix(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG), requestSize);
        config.put(StreamsConfig.consumerPrefix(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG), Objects.requireNonNull(getStreamsOffsetReset()));
        config.put(StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG, getMetricsRecodingLevel());
        config.put(StreamsConfig.STATE_DIR_CONFIG, getStreamsStateDir());
        config.put(ProducerConfig.LINGER_MS_CONFIG, 10);
        config.put(ProducerConfig.BATCH_SIZE_CONFIG, 16_384 * 4);
        config.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");

        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());

        config.put(StreamsConfig.ROCKSDB_CONFIG_SETTER_CLASS_CONFIG, OrmRocksDBConfig.class);

        return config;
    }

    private static String getMetricsRecodingLevel() {
        return SystemUtil.getEnvironmentVariable("STREAMS_METRICS_RECORDING_LEVEL", "INFO");
    }

    private static String getStreamsApplicationId(String defaultApplicationId) {
        return SystemUtil.getEnvironmentVariable("STREAMS_APPLICATION_ID", defaultApplicationId);
    }

    private static String getStreamsClientId(String defaultApplicationId) {
        return SystemUtil.getEnvironmentVariable("STREAMS_CLIENT_ID", getStreamsApplicationId(defaultApplicationId) + "-client");
    }

    private static String getKafkaBootstrapServers() {
        String defaultHosts = "kafka-1.orm-dev-service:9092,kafka-2.orm-dev-service:9092,kafka-3.orm-dev-service:9092";

        return SystemUtil.getEnvironmentVariable("BOOTSTRAP_SERVERS", defaultHosts);
    }

    private static Integer getNumKafkaBrokersInOrmCluster() {
        return SystemUtil.getStreamsOffsetReset("BROKERS_IN_CLUSTER", getKafkaBootstrapServers().contains("localhost") ? 1 : 3);
    }

    private static String getStreamsStateDir() {
        return SystemUtil.getEnvironmentVariable("STREAMS_STATE_DIR", new File(System.getProperty("java.io.tmpdir"), "kafka-streams").getAbsolutePath());
    }

    private static String getStreamsOffsetReset() {
        Offset offset = Offset.fromString(SystemUtil.getEnvironmentVariable("STREAMS_OFFSET_RESET", Offset.EARLIEST.value()));

        if (offset != null) {
            return offset.value();
        }

        return Offset.EARLIEST.value();
    }
}
