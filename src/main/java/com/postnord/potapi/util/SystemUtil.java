package com.postnord.potapi.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemUtil {
    private static final Logger logger = LoggerFactory.getLogger(SystemUtil.class);

    public static String getEnvironmentVariable(String key, String defaultValue) {
        if (key == null) {
            return null;
        }

        String envValue = StringUtils.trimToNull(System.getenv(key));
        String value = envValue == null ? StringUtils.trimToNull(defaultValue) : envValue;

        if (logger.isDebugEnabled()) {
            logger.debug("Environment variable {}={}", key, value);
        }

        return value;
    }

    static Integer getStreamsOffsetReset(String key, Integer defaultValue) {
        String envValue = getEnvironmentVariable(key, null);
        Integer value = envValue == null ? defaultValue : Integer.decode(envValue);

        if (logger.isDebugEnabled()) {
            logger.debug("Environment variable {}={}", key, value);
        }

        return value;
    }
}
