package com.postnord.potapi.util;

import org.apache.kafka.streams.state.RocksDBConfigSetter;
import org.rocksdb.*;

import java.util.Map;

public class OrmRocksDBConfig implements RocksDBConfigSetter {
    private static final CompressionType COMPRESSION_TYPE = CompressionType.SNAPPY_COMPRESSION;


    private static final int BITS_PER_KEY = 10;

    @Override
    public void setConfig(String storeName, Options options, Map<String, Object> configs) {
        final BlockBasedTableConfig tableConfig = new BlockBasedTableConfig();

        // Do not let index and filter blocks grow unbounded.
        // See <https://github.com/facebook/rocksdb/wiki/Block-Cache#caching-index-and-filter-blocks>
        tableConfig.setCacheIndexAndFilterBlocks(true);
        tableConfig.setPinL0FilterAndIndexBlocksInCache(true);

        // Bloom filter
        tableConfig.setFilter(new BloomFilter(BITS_PER_KEY, true));
        tableConfig.setBlockRestartInterval(4);

        options.setTableFormatConfig(tableConfig);

        // Optimize for read speed
        options.allowMmapReads();
        options.setCompressionType(COMPRESSION_TYPE);
        options.level0FileNumCompactionTrigger();
        options.setMaxBackgroundFlushes(8);
        options.setMaxBackgroundCompactions(8);
        options.setMaxSubcompactions(4);
        options.setMaxOpenFiles(-1);

        options.setEnableWriteThreadAdaptiveYield(true);
        options.setUseAdaptiveMutex(true);

        options.setInfoLogLevel(InfoLogLevel.INFO_LEVEL);
        options.setStatsDumpPeriodSec(60);
        options.setDbLogDir("/tmp/log");
    }

}
